﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Koer> koerad = new List<Koer>()
            {
                new Koer {Nimi = "Pontu", Tõug = "krants", Vanus = 8},
                new Koer {Nimi = "Polla", Tõug = "bolonka", Vanus = 18},
                new Koer {Nimi = "Pauka", Tõug = "taks", Vanus = 10},
                new Koer {Nimi = "Pitsu", Tõug = "nähvits", Vanus = 1},
                new Koer {Nimi = "Pallu", Tõug = "bulgog", Vanus = 3},
                new Koer {Nimi = "Coffi", Tõug = "lambakoer", Vanus = 8},
                new Koer {Nimi = "Lulla", Tõug = "kaukaaslane", Vanus = 2},
                new Koer {Nimi = "Muki", Tõug = "koerake", Vanus = 6}
            };

            Console.WriteLine("Kutsikad:");
            foreach (var x in koerad) if (x.Grupp() == "kutsikas") Console.WriteLine($"\t{x}");
            Console.WriteLine("Veteranid:");
            foreach (var x in koerad) if (x.Grupp() == "veteran") Console.WriteLine($"\t{x}");
            Console.WriteLine("Koerad:");
            foreach (var x in koerad) if (x.Grupp() == "koer") Console.WriteLine($"\t{x}");

        }
    }

    class Koer
    {
        public static int KutsikaPiir = 2;
        public static int VeteraniPiir = 12;

        public string Nimi;
        public string Tõug;
        public int Vanus;

        public string Grupp()
        {
            return 
                Vanus <= KutsikaPiir ? "kutsikas" : 
                Vanus >= VeteraniPiir ? "veteran" : "koer";
        }

        public override string ToString()
        {
            return $"{Tõug} {Nimi} {Vanus} aastat vana"; 
        }
    }
}
